package eval

import (
	"context"
	"regexp"
	"strings"

	"github.com/PaesslerAG/gval"
)

func getHVal(expr string) (HVal, bool) {
	parts := strings.Split(expr, "=")
	if len(parts) != 2 {
		return "", false
	}

	left := strings.Trim(parts[0], " ")
	right := strings.Trim(parts[1], " ")

	if left != "H" {
		return "", false
	}

	switch right {
	case "M":
		return M, true
	case "P":
		return P, true
	case "T":
		return T, true
	}

	return "", false
}

func getKExp(expr string) (string, bool) {
	parts := strings.Split(expr, " = ")
	if len(parts) != 2 {
		return "", false
	}

	left := strings.Trim(parts[0], " ")
	right := strings.Trim(parts[1], " ")

	if left != "K" {
		return "", false
	}
	if !regexp.MustCompile(`[0-9DEF+\-/*()]+`).MatchString(right) {
		return "", false
	}

	return right, true
}

func evaluateH(rule string, a, b, c bool) (bool, error) {
	evaluable, err := gval.PropositionalLogic().NewEvaluable(rule)
	if err != nil {
		return false, err
	}

	return evaluable.EvalBool(context.Background(),
		map[string]interface{}{
			"A": a,
			"B": b,
			"C": c,
		})
}

func evaluateK(rule string, d float64, e, f int64) (float64, error) {
	evaluable, err := gval.Arithmetic().NewEvaluable(rule)
	if err != nil {
		return 0, err
	}

	return evaluable.EvalFloat64(context.Background(),
		map[string]interface{}{
			"D": d,
			"F": f,
			"E": e,
		})
}
