package eval

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// # Base
// A && B && !C => H = M
// A && B && C => H = P
// !A && B && C => H = T
// [other] => [error]
// H = M => K = D + (D * E / 10)
// H = P => K = D + (D * (E - F) / 25.5)
// H = T => K = D - (D * F / 30)

// # Custom 1
// H = P => K = 2 * D + (D * E / 100)

// # Custom 2
// A && B && !C => H = T
// A && !B && C => H = M
// H = M => K = F + D + (D * E / 100)

func TestRulesSet_AddRule(t *testing.T) {
	cases := []struct {
		rule     string
		hasError bool
	}{
		{rule: "A && B && !C => H = M", hasError: false},
		{rule: "A && B && C => H = P", hasError: false},
		{rule: "!A && B && C => H = T", hasError: false},
		{rule: "H = M => K = D + (D * E / 10)", hasError: false},
		{rule: "H = P => K = D + (D * (E - F) / 25.5)", hasError: false},
		{rule: "H = T => K = D - (D * F / 30)", hasError: false},
		{rule: "H = P => K = 2 * D + (D * E / 100)", hasError: false},
		{rule: "A && B && !C => H = T", hasError: false},
		{rule: "A && !B && C => H = M", hasError: false},
		{rule: "H = M => K = F + D + (D * E / 100)", hasError: false},
	}

	set := NewRulesSet()
	for _, testCase := range cases {
		err := set.AddRule(testCase.rule)
		if testCase.hasError {
			assert.Errorf(t, err, "assert for case [%s] failed", testCase.rule)
		} else {
			assert.NoErrorf(t, err, "assert for case [%s] failed", testCase.rule)
		}

	}
}

func TestRulesSet_EvaluateInput(t *testing.T) {
	rules := []struct {
		rule     string
		hasError bool
		in       Input
		out      Output
	}{
		{rule: "A && B && !C => H = M", hasError: false},
		{rule: "A && B && C => H = P", hasError: false},
		{rule: "!A && B && C => H = T", hasError: false},
		{rule: "H = M => K = D + (D * E / 10)", hasError: false},
		{rule: "H = P => K = D + (D * (E - F) / 25.5)", hasError: false},
		{rule: "H = T => K = D - (D * F / 30)", hasError: false},
		{rule: "H = P => K = 2 * D + (D * E / 100)", hasError: false},
		{rule: "A && B && !C => H = T", hasError: false},
		{rule: "A && !B && C => H = M", hasError: false},
		{rule: "H = M => K = F + D + (D * E / 100)", hasError: false},
	}

	// 0 = A && B && !C  -> T
	// 1 = A && B && C  -> P
	// 2 = !A && B && C  -> T
	// 3 = A && !B && C  -> M

	// 0 = M -> F + D + (D * E / 100) | 10 + 5 + (5 * 400 / 100) = 35
	// 1 = P -> 2 * D + (D * E / 100) | 2 + 5 + (5 * 400 / 100) = 27
	// 2 = T -> D - (D * F / 30) | 3 - (3 * 10 / 30)

	dataSet := []struct {
		in       Input
		out      Output
		hasError bool
	}{
		{
			in:       Input{A: false, B: false, C: false, D: 0, F: 0, E: 0},
			out:      Output{H: "", K: 0},
			hasError: true,
		},

		{
			in:       Input{A: true, B: true, C: false, D: 0, F: 0, E: 0},
			out:      Output{H: T, K: 0},
			hasError: false,
		},
		{
			in:       Input{A: true, B: true, C: true, D: 0, F: 0, E: 0},
			out:      Output{H: P, K: 0},
			hasError: false,
		},
		{
			in:       Input{A: false, B: true, C: true, D: 0, F: 0, E: 0},
			out:      Output{H: T, K: 0},
			hasError: false,
		},
		{
			in:       Input{A: true, B: false, C: true, D: 0, F: 0, E: 0},
			out:      Output{H: M, K: 0},
			hasError: false,
		},

		// 0 = M -> F + D + (D * E / 100) | 10 + 5 + (5 * 400 / 100) = 35
		// 1 = P -> 2 * D + (D * E / 100) | 2 + 5 + (5 * 400 / 100) = 30
		// 2 = T -> D - (D * F / 30) | 3 - (3 * 10 / 30) = 2

		{
			in:       Input{A: true, B: true, C: false, D: 3, F: 10, E: 0},
			out:      Output{H: T, K: 2},
			hasError: false,
		},
		{
			in:       Input{A: true, B: true, C: true, D: 5, F: 0, E: 400},
			out:      Output{H: P, K: 30},
			hasError: false,
		},
		{
			in:       Input{A: false, B: true, C: true, D: 3, F: 10, E: 0},
			out:      Output{H: T, K: 2},
			hasError: false,
		},
		{
			in:       Input{A: true, B: false, C: true, D: 5, F: 10, E: 400},
			out:      Output{H: M, K: 35},
			hasError: false,
		},
	}

	set := NewRulesSet()
	for _, testCase := range rules {
		err := set.AddRule(testCase.rule)
		if testCase.hasError {
			assert.Errorf(t, err, "assert for case [%s] failed", testCase.rule)
		} else {
			assert.NoErrorf(t, err, "assert for case [%s] failed", testCase.rule)
		}
	}

	for _, testCase := range dataSet {
		res, err := set.EvaluateInput(testCase.in)
		if testCase.hasError {
			assert.Error(t, err)
		} else {
			assert.NoError(t, err)
		}

		assert.Equal(t, testCase.out, res)
	}
}
