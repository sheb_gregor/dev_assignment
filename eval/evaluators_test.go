package eval

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_getHVal(t *testing.T) {
	tests := []struct {
		expr string
		val  HVal
		ok   bool
	}{
		{expr: "H = M", val: M, ok: true},
		{expr: "H=M", val: M, ok: true},
		{expr: "H != T", val: "", ok: false},
		{expr: "!T", val: "", ok: false},
	}

	for _, tt := range tests {
		t.Run(tt.expr, func(t *testing.T) {
			val, ok := getHVal(tt.expr)
			if val != tt.val {
				t.Errorf("getHVal() got = %v, res %v", val, tt.val)
			}
			if ok != tt.ok {
				t.Errorf("getHVal() got1 = %v, res %v", ok, tt.ok)
			}
		})
	}
}

func Test_getKExp(t *testing.T) {
	type args struct {
		expr string
	}
	tests := []struct {
		expr string
		res  string
		ok   bool
	}{
		{expr: "K = D + (D * E / 10)", res: "D + (D * E / 10)", ok: true},
		{expr: "K = D + (D * (E - F) / 25.5)",  res: "D + (D * (E - F) / 25.5)", ok: true},
		{expr: "K = D - (D * F / 30)",  res: "D - (D * F / 30)", ok: true},
		{expr: "K = 2 * D + (D * E / 100)",  res: "2 * D + (D * E / 100)", ok: true},
		{expr: "K = F + D + (D * E / 100)",  res: "F + D + (D * E / 100)", ok: true},
		{expr: "K = F + D - (D * E / 100)",  res: "F + D - (D * E / 100)", ok: true},
		{expr: "",  res: "", ok: false},
		{expr: "test must fail",  res: "", ok: false},
	}
	for _, tt := range tests {
		t.Run(tt.expr, func(t *testing.T) {
			res, ok := getKExp(tt.expr)
			if res != tt.res {
				t.Errorf("getKExp() got = %v, want %v", res, tt.res)
			}
			if ok != tt.ok {
				t.Errorf("getKExp() got = %v, want %v", ok, tt.ok)
			}
		})
	}
}

func Test_evaluateH(t *testing.T) {
	type in struct {
		a, b, c bool
	}

	type out struct {
		match, hasError bool
	}

	rules := []struct {
		rule string
		in   in
		out  out
	}{
		{rule: "A && B && !C", in: in{a: true, b: true, c: true}, out: out{match: false, hasError: false}},
		{rule: "A && B && !C", in: in{a: true, b: false, c: true}, out: out{match: false, hasError: false}},
		{rule: "A && B && !C", in: in{a: false, b: false, c: true}, out: out{match: false, hasError: false}},
		{rule: "A && B && !C", in: in{a: true, b: true, c: false}, out: out{match: true, hasError: false}},

		{rule: "A && B && C", in: in{a: true, b: true, c: false}, out: out{match: false, hasError: false}},
		{rule: "A && B && C", in: in{a: true, b: true, c: true}, out: out{match: true, hasError: false}},

		{rule: "A && !B && C", in: in{a: true, b: true, c: true}, out: out{match: false, hasError: false}},
		{rule: "A && !B && C", in: in{a: true, b: false, c: true}, out: out{match: true, hasError: false}},
	}

	for _, testCase := range rules {
		match, err := evaluateH(testCase.rule, testCase.in.a, testCase.in.b, testCase.in.c)
		if testCase.out.hasError {
			assert.Errorf(t, err, "assert for case [%s] failed", testCase.rule)
		} else {
			assert.NoErrorf(t, err, "assert for case [%s] failed", testCase.rule)
		}
		assert.Equalf(t, testCase.out.match, match, "assert for case [%s] failed", testCase.rule)
	}
}

func Test_evaluateK(t *testing.T) {
	type in struct {
		d float64
		e int64
		f int64
	}

	type out struct {
		k        float64
		hasError bool
	}

	rules := []struct {
		rule string
		in   in
		out  out
	}{
		{rule: "D + (D * E / 10)", in: in{d: 10, e: 2, f: 0}, out: out{k: 12, hasError: false}},
		// 5.1 + (5.1 * (6 - 5) / 25.5) = 5.3
		{rule: "D + (D * (E - F) / 25.5)", in: in{d: 5.1, e: 6, f: 5}, out: out{k: 5.3, hasError: false}},
		// 3 - (3 * 10 / 30) = 2
		{rule: "D - (D * F / 30)", in: in{d: 3, e: 0, f: 10}, out: out{k: 2, hasError: false}},
		// 2 * 2 + (2 * 5 / 100) = 4.1
		{rule: "2 * D + (D * E / 100)", in: in{d: 2, e: 5, f: 0}, out: out{k: 4.1, hasError: false}},
		{rule: "F + D + (D * E / 100)", in: in{d: 2, e: 5, f: 20}, out: out{k: 22.1, hasError: false}},
		{rule: "F + D == (D * E / 100)", in: in{d: 2, e: 5, f: 20}, out: out{k: 0, hasError: true}},

		{rule: "", in: in{d: 2, e: 5, f: 20}, out: out{k: 0, hasError: true}},
		{rule: "test must fail", in: in{d: 2, e: 5, f: 20}, out: out{k: 0, hasError: true}},
	}

	for _, testCase := range rules {
		k, err := evaluateK(testCase.rule, testCase.in.d, testCase.in.e, testCase.in.f)
		if testCase.out.hasError {
			assert.Errorf(t, err, "assert for case [%s] failed", testCase.rule)
		} else {
			assert.NoErrorf(t, err, "assert for case [%s] failed", testCase.rule)
		}
		assert.Equalf(t, testCase.out.k, k, "assert for case [%s] failed", testCase.rule)
	}
}
