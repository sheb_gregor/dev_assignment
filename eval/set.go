package eval

import (
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

const TokenErgo = "=>"

type Input struct {
	A bool    `json:"a"`
	B bool    `json:"b"`
	C bool    `json:"c"`
	D float64 `json:"d"`
	F int64   `json:"f"`
	E int64   `json:"e"`
}

type Output struct {
	H HVal    `json:"h"`
	K float64 `json:"k"`
}

type HVal string

const (
	M HVal = "M"
	P HVal = "P"
	T HVal = "T"
)

type RulesSet struct {
	rules []string

	kExpressions    map[HVal]string
	hValExpressions map[string]HVal
}

func NewRulesSet() *RulesSet {
	return &RulesSet{
		rules: []string{},

		kExpressions:    map[HVal]string{},
		hValExpressions: map[string]HVal{},
	}
}

func (set *RulesSet) AddRules(rules ...string) error {
	for _, rule := range rules {
		if err := set.AddRule(rule); err != nil {
			return err
		}
	}
	return nil
}

func (set *RulesSet) AddRule(rule string) error {
	parts := strings.Split(rule, TokenErgo)
	if len(parts) != 2 {
		return errors.New("invalid syntax")
	}

	left := parts[0]
	right := parts[1]

	abcRule := regexp.MustCompile(`[ABC!&]+`).MatchString(left)
	hRule := regexp.MustCompile(`[H=MPT]+`).MatchString(left)

	switch {
	case abcRule:
		val, ok := getHVal(right)
		if !ok {
			return errors.New("invalid right side: " + right)
		}

		set.hValExpressions[left] = val

	case hRule:
		val, ok := getHVal(left)
		if !ok {
			return errors.New("invalid left side: " + left)
		}

		expr, ok := getKExp(right)
		if !ok {
			return errors.New("invalid right side: " + right)
		}

		set.kExpressions[val] = expr
	}

	set.rules = append(set.rules, rule)
	return nil
}

func (set *RulesSet) EvaluateInput(input Input) (Output, error) {
	var err error

	out := Output{}
	var ok bool

	for expr, val := range set.hValExpressions {
		ok, err = evaluateH(expr, input.A, input.B, input.C)
		if err != nil {
			return Output{}, errors.Wrap(err, "unable to evaluate input to H value")
		}

		if ok {
			out.H = val
			break
		}
	}
	if !ok {
		return Output{}, errors.New("H variant not found")
	}

	expr, ok := set.kExpressions[out.H]
	if !ok {
		return Output{}, errors.New("K expression not found")
	}

	out.K, err = evaluateK(expr, input.D, input.E, input.F)
	return out, err
}
