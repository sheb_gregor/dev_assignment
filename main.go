package main

import (
	"log"
	"os"

	"dev_assignment/cmd"
	"dev_assignment/config"

	"github.com/urfave/cli"
)

func main() {
	appInfo := config.AppInfo()

	app := cli.NewApp()
	app.Usage = "A " + appInfo.Name + " service"
	app.Version = appInfo.Version + ", build " + appInfo.Build
	app.Flags = cmd.GetFlags()
	app.Commands = cmd.GetCommands()
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
