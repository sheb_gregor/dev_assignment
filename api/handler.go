package api

import (
	"net/http"
	"sync"

	"dev_assignment/eval"

	"github.com/lancer-kit/armory/api/httpx"
	"github.com/lancer-kit/armory/api/render"
)

type Handler struct {
	sync.Mutex
	rulesSet *eval.RulesSet
}

func NewHandler() *Handler {
	return &Handler{
		rulesSet: eval.NewRulesSet(),
	}
}

type RuleRequest struct {
	Rules []string `json:"rules"`
}

func (handler *Handler) AddRule(w http.ResponseWriter, r *http.Request) {
	request := new(RuleRequest)

	err := httpx.ParseJSONBody(r, &request)
	if err != nil {
		render.BadRequest(w, err)
		return
	}

	handler.Lock()
	defer handler.Unlock()
	err = handler.rulesSet.AddRules(request.Rules...)
	if err != nil {
		render.BadRequest(w, err)
		return
	}

	render.ResultSuccess.Render(w)
}

func (handler *Handler) Evaluate(w http.ResponseWriter, r *http.Request) {
	request := new(eval.Input)

	err := httpx.ParseJSONBody(r, &request)
	if err != nil {
		render.BadRequest(w, err)
		return
	}

	handler.Lock()
	defer handler.Unlock()

	out, err := handler.rulesSet.EvaluateInput(*request)
	if err != nil {
		render.BadRequest(w, err)
		return
	}

	render.Success(w, out)
}
