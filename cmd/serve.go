package cmd

import (
	"dev_assignment/api"
	"dev_assignment/cmd/modules"
	"dev_assignment/config"

	"github.com/lancer-kit/armory/log"
	"github.com/lancer-kit/uwe/v2"
	"github.com/urfave/cli"
)

func serveCmd() cli.Command {
	var serveCommand = cli.Command{
		Name:   "serve",
		Usage:  "starts " + config.ServiceName + " workers",
		Action: serveAction,
	}

	return serveCommand
}

func serveAction(c *cli.Context) error {
	cfg, err := modules.Init(c)
	if err != nil {
		return cli.NewExitError(err, 1)
	}

	logger := log.Get().WithField("app", config.ServiceName)
	logger = logger.WithField("app_layer", "workers")

	chief := uwe.NewChief()
	chief.UseDefaultRecover()
	chief.EnableServiceSocket(config.AppInfo())
	chief.SetEventHandler(uwe.LogrusEventHandler(logger))

	chief.AddWorkers(map[uwe.WorkerName]uwe.Worker{
		config.APIServer: api.NewLogsServer(cfg, logger.WithField("worker", config.APIServer)),
	})

	chief.Run()

	return nil
}
