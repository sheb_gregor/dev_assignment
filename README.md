# Go Web-Service

This is a project example build with Lancer-Kit tool set.

#### What service do?

- Listen on ports `:5000`

- Receive *Evaluation Rule* as JSON array logs on `POST <host>:5000/rules`

- Evaluate Input data according to set *Rules* `POST <host>:5000/rules`


#### Quick start

1. Clone this repo:

```shell script
git clone https://gitlab.com/sheb_gregor/dev_assignment
cd dev_assignment
```

2. Prepare a local configuration:

```shell script
## here is secrets and other env vars
cp ./env/tmpl.env ./env/local.env

## here is configuration details
cp ./env/tmpl.config.yaml ./env/local.config.yaml
```

3. Build docker image:

```shell script
make build_docker image=dev_assignment config=local
```

4. Start all:

```shell script
docker-compose up -d
```

## Development

### Build 

```shell script
./build.sh
```
 
### Test 

```shell script
go test ./...
```


### API

Check [request file](./api/requests.http) for details and examples

- Add rules:

```http request
###

POST http://localhost:5000/rules
Accept: */*
Content-Type: application/json

{
  "rules": [
    "A && B && !C => H = M",
    "A && B && C => H = P",
    "!A && B && C => H = T",
    "H = M => K = D + (D * E / 10)",
    "H = P => K = D + (D * (E - F) / 25.5)",
    "H = T => K = D - (D * F / 30)",
    "H = P => K = 2 * D + (D * E / 100)",
    "A && B && !C => H = T",
    "A && !B && C => H = M",
    "H = M => K = F + D + (D * E / 100)"
  ]
}

```

- Evaluate input:

```http request
POST http://localhost:5000/evaluate
Accept: */*
Content-Type: application/json

{
  "a": true,
  "b": true,
  "c": false,
  "d": 3,
  "f": 10,
  "e": 0
}
```


